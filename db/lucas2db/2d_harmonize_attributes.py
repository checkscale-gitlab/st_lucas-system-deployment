#!/usr/bin/env python3

import os

from lib.change_attributes import ChangeAttributes

class RenameAttributes(ChangeAttributes):
    item = "new_name"
    
    @staticmethod    
    def build_sql(*args):
        print('ALTER TABLE lucas{} RENAME COLUMN {} TO {};'.format(
            *args)
        )

if __name__ == "__main__":
    cha = RenameAttributes()
    cha.run()
