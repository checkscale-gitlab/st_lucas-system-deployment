#!/usr/bin/env python3

import os
import argparse

def scan_dir(data_dir):
    start=None
    end=None
    for f in os.listdir(data_dir):
       year = int(f.split('_')[1])
       if start is None:
           start = end = year
       else:
           if year < start:
               start=year
           elif year > end:
               end=year

    if os.environ.get("START_YEAR") is None:
       print("START_YEAR={}".format(start))
    if os.environ.get("END_YEAR") is None:
       print("END_YEAR={}".format(end))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('data_dir', metavar='data_dir', type=str,
                        help='Path to data directory')

    args = parser.parse_args()

    scan_dir(args.data_dir)
