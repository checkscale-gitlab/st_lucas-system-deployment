import os
import copy
from collections import OrderedDict

from osgeo import ogr

from lib.parser_cli import read_groups

class CreateViews:
    exclude = []
    topics_additional = []

    def __init__(self, columns, groups, table, pkey=None):
        # keep only selected geometry column
        cols = columns.keys()
        for col in list(cols):
            if col.startswith(('geog', 'geom')):
                del columns[col]

        self.columns = columns
        self.groups = read_groups(groups)
        self.table = table
        self.pkey = pkey
        self.geom = "geom"

    def _read_groups(self, filename):
        with open(filename) as fd:
            data = json.load(fd)
        groups = {}
        for item in data["thematic_attributes"].values():
            for group, topics in item.items():
                if group not in groups.keys():
                    groups[group] = set()
                groups[group].update(
                    list(map(lambda x: x.lower(), topics))
                )

        return groups

    def _get_columns(self, name):
        return [name]
        
    def build_sql(self):
        print("SET search_path TO {},public;".format(os.environ["POSTGRES_SCHEMA"]))

        print("BEGIN;")
        for group, topics in self.groups.items():
            topics.update(self.topics_additional)
            self._create_view(group, topics)
        # all
        self._create_view()

        print("COMMIT")

    def _create_view(self, group=None, topics=None):
        if group:
            view = '{}_{}'.format(self.table, group.lower())
        else:
            view = self.table

        self._create_view_sql(view, topics)
        if group:
            self._create_view_sql(view, topics, base_only=True)

    def _collect_columns(self, topics, base_only):
        columns_view = []
        if self.pkey:
            columns_view.append(self.pkey)
        for name, item in self.columns.items():
            if item["group"].upper() in self.exclude:
                continue
            if item["group"] and \
               (topics is None or (item["group"] == "default" or item["group"] in topics)) and \
               (base_only is False or item["base"] == "1"):
                columns_view.extend(self._get_columns(name))
        columns_view.append(self.geom)

        return columns_view

    def _create_view_sql(self, view, topics, base_only=False):
        print("CREATE VIEW {}.{} AS SELECT {} FROM {}.{};".format(
            os.environ["MAPSERVR_SCHEMA"], view + '_base' if base_only else view,
            ','.join(self._collect_columns(topics, base_only)),
            os.environ["POSTGRES_SCHEMA"], self.table
        ))


        
