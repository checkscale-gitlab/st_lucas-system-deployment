--
-- LUCAS 2009-2015
--
-- LC1_PERC
ALTER TABLE lucas${YEAR} ADD COLUMN lc1_perc_cls varchar;
UPDATE lucas${YEAR} SET lc1_perc_cls = lc1_perc;
UPDATE lucas${YEAR} SET lc1_perc = '10'  WHERE lc1_perc_cls = '1';
UPDATE lucas${YEAR} SET lc1_perc = '25'  WHERE lc1_perc_cls = '2';
UPDATE lucas${YEAR} SET lc1_perc = '50'  WHERE lc1_perc_cls = '3';
UPDATE lucas${YEAR} SET lc1_perc = '75'  WHERE lc1_perc_cls = '4';
UPDATE lucas${YEAR} SET lc1_perc = '100' WHERE lc1_perc_cls = '5';
-- LC2_PERC
ALTER TABLE lucas${YEAR} ADD COLUMN lc2_perc_cls varchar;
UPDATE lucas${YEAR} SET lc2_perc_cls = lc2_perc;
UPDATE lucas${YEAR} SET lc2_perc = '10'  WHERE lc2_perc_cls = '1';
UPDATE lucas${YEAR} SET lc2_perc = '25'  WHERE lc2_perc_cls = '2';
UPDATE lucas${YEAR} SET lc2_perc = '50'  WHERE lc2_perc_cls = '3';
UPDATE lucas${YEAR} SET lc2_perc = '75'  WHERE lc2_perc_cls = '4';
UPDATE lucas${YEAR} SET lc2_perc = '100' WHERE lc2_perc_cls = '5';
-- SOIL_STONES_PERC
ALTER TABLE lucas${YEAR} ADD COLUMN soil_stones_perc_cls varchar;
UPDATE lucas${YEAR} SET soil_stones_perc_cls = soil_stones_perc;
UPDATE lucas${YEAR} SET soil_stones_perc_cls = '1' WHERE soil_stones_perc = '0';
UPDATE lucas${YEAR} SET soil_stones_perc = '5'   WHERE soil_stones_perc_cls = '1';
UPDATE lucas${YEAR} SET soil_stones_perc = '20'  WHERE soil_stones_perc_cls = '2';
UPDATE lucas${YEAR} SET soil_stones_perc = '40'  WHERE soil_stones_perc_cls = '3';
UPDATE lucas${YEAR} SET soil_stones_perc = '75'  WHERE soil_stones_perc_cls = '4';
