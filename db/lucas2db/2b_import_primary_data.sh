#!/bin/bash -ex

if [ -n "$1" ]; then
    INPUT_DIR=$1
else
    echo "Input directory not defined"
    exit 1
fi

CPL_LOG=/dev/null # suppress warnings

# LUCAS point data
for file in $INPUT_DIR/points/*.[cC][sS][vV]; do
    dir="$(dirname $file)"
    year=`echo $(basename $file) | cut -d'_' -f2`
    table="lucas${year}"
    echo "Importing $file -> $table..."
    if [ $table = "lucas2006" ] ; then
        X="x_laea"
        Y="y_laea"
        S_SRS=3035
        T_SRS=4326
    else
        X="th_long"
        Y="th_lat"
        S_SRS=4326
        T_SRS=4326
    fi

    grep "\S" $file | grep -v 'rows selected' | \
        ogr2ogr -f PostgreSQL -lco GEOMETRY_NAME=geog_th -lco GEOM_TYPE="geography" \
            -nln ${POSTGRES_SCHEMA}.$table -oo X_POSSIBLE_NAMES=$X -oo Y_POSSIBLE_NAMES=$Y \
            -s_srs EPSG:$S_SRS -t_srs EPSG:$T_SRS \
            -append "PG:dbname=${POSTGRES_DB}" CSV:/vsistdin/
done

# LUCAS grid
ogr2ogr -f PostgreSQL -lco GEOMETRY_NAME=geog_th -lco GEOM_TYPE="geography" \
        -nln ${POSTGRES_SCHEMA}.grid -oo X_POSSIBLE_NAMES=X_WGS84 -oo Y_POSSIBLE_NAMES=Y_WGS84 \
        -a_srs EPSG:4326 \
        -overwrite "PG:dbname=${POSTGRES_DB}" ${INPUT_DIR}/grid/*.csv

exit 0
