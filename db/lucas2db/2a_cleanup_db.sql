-- clean-up (remove schemas introduced by docker image)
DROP SCHEMA tiger CASCADE;
DROP SCHEMA tiger_data CASCADE;
DROP SCHEMA topology CASCADE;
