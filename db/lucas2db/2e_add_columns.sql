-- LC
ALTER TABLE lucas${YEAR} ADD COLUMN lc1_h varchar;
ALTER TABLE lucas${YEAR} ADD COLUMN lc2_h varchar;
-- LU
ALTER TABLE lucas${YEAR} ADD COLUMN lu1_h varchar;
ALTER TABLE lucas${YEAR} ADD COLUMN lu2_h varchar;
-- Missing translation to level 3
ALTER TABLE lucas${YEAR} ADD COLUMN lc1_h_l3_missing varchar NULL;
ALTER TABLE lucas${YEAR} ADD COLUMN lc2_h_l3_missing varchar NULL;
ALTER TABLE lucas${YEAR} ADD COLUMN lc1_h_l3_missing_level int NULL;
ALTER TABLE lucas${YEAR} ADD COLUMN lc2_h_l3_missing_level int NULL;
