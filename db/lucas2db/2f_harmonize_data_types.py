#!/usr/bin/env python3

import os

from lib.change_attributes import ChangeAttributes

class RetypeAttributes(ChangeAttributes):
    item = "data_type"
    
    @staticmethod
    def build_sql(*args):
        if args[2] == "date":
            print("ALTER TABLE lucas{0} ALTER COLUMN {1} TYPE {2} using to_date({1}, 'DD/MM/YY');".format(
                *args)
            )
        else:
            print("ALTER TABLE lucas{0} ALTER COLUMN {1} TYPE {2} using {1}::{2};".format(
                *args)
            )

if __name__ == "__main__":
    cha = RetypeAttributes()
    cha.run()
