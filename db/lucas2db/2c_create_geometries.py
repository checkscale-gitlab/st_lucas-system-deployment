import os
import sys

from lib.parser_cli import Parser

# geog_th already built by 02b
# new geographies
#  * geog_gps: built from gps_lat and gps_long
# new geometries
#  * geom_gps: transformed from geog_gps to EPSG 3035
#  * geom_thr: geog_th snapped to the grid and transformed to EPSG 3035
#  * geom_repr_area: in 2018 built from cprnc_lc1n|s|e|w attributes,
#                    otherwise built from obs_radius
# distance attributes:
#  * obs_dist: distance between geom_gps and geom_thr

def create_geometries(start_year, end_year):
    for year in range(start_year, end_year+1, 3):
        # geog_gps (4326)
        print("ALTER TABLE lucas{0} ALTER COLUMN {1} TYPE {2} using {1}::{2};".format(year, "gps_long", "double precision"))
        print("ALTER TABLE lucas{0} ALTER COLUMN {1} TYPE {2} using {1}::{2};".format(year, "gps_lat", "double precision"))
        print("UPDATE lucas{0} SET gps_long = gps_long * -1 FROM (SELECT ogc_fid as id from lucas{0} WHERE gps_ew = '2') as S WHERE ogc_fid = s.id;".format(year))
        print("ALTER TABLE lucas{} ADD COLUMN geog_gps geography(point, 4326);".format(year))
        print("UPDATE lucas{} SET geog_gps = st_setsrid(st_geomfromtext('POINT(' || gps_long || ' ' || gps_lat || ')'), 4326)::geography WHERE gps_proj = '1';".format(year))
        # geom_gps (3035)
        print("ALTER TABLE lucas{} ADD COLUMN geom_gps geometry(point, 3035);".format(year))
        print("UPDATE lucas{} SET geom_gps = st_transform(geog_gps::geometry, 3035);".format(year))
        # geom_thr (3035)
        print("ALTER TABLE lucas{} ADD COLUMN geom_thr geometry(point, 3035);".format(year))
        print("UPDATE lucas{} SET geom_thr = st_transform(geog_th::geometry, 3035);".format(year))
        print("UPDATE lucas{} SET geom_thr = st_setsrid(st_point((st_x(geom_thr) / 1000)::int * 1000, (st_y(geom_thr) / 1000)::int * 1000), 3035);".format(year))
        # geom (3035)
        print("ALTER TABLE lucas{} ADD COLUMN geom geometry (point, 3035);".format(year))
        print("UPDATE lucas{} SET geom = geom_gps WHERE geom_gps IS NOT NULL;".format(year))
        print("UPDATE lucas{} SET geom = geom_thr WHERE geom_gps IS NULL;".format(year))

        # geom_repr_area (3035)
        print("ALTER TABLE lucas{} ADD COLUMN geom_repr_area geometry(polygon, 3035);".format(year))
        if year < 2018:
            print("UPDATE lucas{} SET geom_repr_area = st_buffer(geom_gps, 1.5) WHERE obs_radius = '1';".format(year))
            print("UPDATE lucas{} SET geom_repr_area = st_buffer(geom_gps, 20) WHERE obs_radius = '2';".format(year))
        else:
            print("UPDATE lucas{y} SET geom_repr_area = s.geom FROM (SELECT "
                  "point_id, st_makeenvelope(st_x(geom_gps)-cprnc_lc1w::int, st_y(geom_gps)-cprnc_lc1s::int, "
                  "st_x(geom_gps)+cprnc_lc1e::int, st_y(geom_gps)+cprn_lc1n::int) AS geom "
                  "FROM data.lucas{y}) AS s WHERE cprn_lc1n != '88' AND lucas{y}.point_id = s.point_id;".format(y=year))

        # compute distance attributes (obs_dist)
        print("UPDATE lucas{} SET obs_dist = NULL;".format(year))        
        print("UPDATE lucas{} SET obs_dist = round(st_distance(geom_thr, geom_gps)) "
              "WHERE geom_gps IS NOT NULL;".format(year))

        # create spatial indices
        print("CREATE index ON lucas{} USING gist (geog_gps);".format(year))
        print("CREATE index ON lucas{} USING gist (geom_gps);".format(year))
        print("CREATE index ON lucas{} USING gist (geom_thr);".format(year))
        print("CREATE index ON lucas{} USING gist (geom);".format(year))
        print("CREATE index ON lucas{} USING gist (geom_repr_area);".format(year))

def remove_outliers(start_year, end_year):
    for year in range(start_year, end_year+1, 3):
        print("""UPDATE lucas{y} SET geom_gps = NULL, geog_gps = NULL WHERE point_id IN (
WITH aoi AS (SELECT st_buffer(st_convexhull(st_collect(geom_thr)), 2000) AS geom FROM grid)
SELECT a.point_id FROM lucas{y} AS a LEFT JOIN aoi ON st_within(a.geom_gps, aoi.geom)
WHERE a.geom_gps IS NOT NULL AND aoi.geom IS NULL);""".format(y=year))

    print("DELETE FROM lucas{y} WHERE st_distance(st_transform(geog_th::geometry, 3035),geom_thr) > 10;".format(y=year))

if __name__ == "__main__":
    parser = Parser(csv_file=False, years=True)

    print("BEGIN;")

    create_geometries(parser.start_year, parser.end_year)

    #
    # grid
    #
    # round coordinates (grid)
    print("""ALTER TABLE grid ADD COLUMN geom_thr geometry(point, 3035);
UPDATE grid SET geom_thr = st_transform(geog_th::geometry, 3035);
UPDATE grid SET geom_thr = st_setsrid(st_point((st_x(geom_thr) / 1000)::int * 1000, (st_y(geom_thr) / 1000)::int * 1000), 3035);"""
    )
    # re-type
    print("ALTER TABLE grid ALTER COLUMN point_id TYPE integer USING point_id::integer;")
    # create indices
    print("""CREATE INDEX on grid (point_id);
CREATE INDEX ON grid USING gist (geom_thr);"""
    )

    remove_outliers(parser.start_year, parser.end_year)

    print("COMMIT")
