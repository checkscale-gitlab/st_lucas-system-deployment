--
-- Trim spaces for LC, LU attributes before harmonization
--
UPDATE lucas${YEAR} SET lc1 = TRIM(BOTH FROM lc1);
UPDATE lucas${YEAR} SET lc2 = TRIM(BOTH FROM lc2);
UPDATE lucas${YEAR} SET lu1 = TRIM(BOTH FROM lu1);
UPDATE lucas${YEAR} SET lu2 = TRIM(BOTH FROM lu2);

-- LC harmonization
UPDATE lucas${YEAR} SET lc1_h = lc1 WHERE lc1_h IS NULL;
UPDATE lucas${YEAR} SET lc2_h = lc2 WHERE lc2_h IS NULL;
-- LU harmonization
UPDATE lucas${YEAR} SET lu1_h = lu1 WHERE lu1_h IS NULL;
UPDATE lucas${YEAR} SET lu2_h = lu2 WHERE lu2_h IS NULL;
