--
-- 2015
--
UPDATE lucas2015 SET lc1_spec = '-1' WHERE lc1_spec = 'C10Z';
UPDATE lucas2015 SET lc1_perc = '1'  WHERE lc1_perc = '2';
UPDATE lucas2015 SET lc1_perc = '2'  WHERE lc1_perc = '3';
UPDATE lucas2015 SET lc1_perc = '3'  WHERE lc1_perc = '4';
UPDATE lucas2015 SET lc1_perc = '4'  WHERE lc1_perc = '5';
UPDATE lucas2015 SET lc1_perc = '5'  WHERE lc1_perc = '6';
UPDATE lucas2015 SET lc1_perc = '5'  WHERE lc1_perc = '7';
UPDATE lucas2015 SET lc2_perc = '1'  WHERE lc2_perc = '2';
UPDATE lucas2015 SET lc2_perc = '2'  WHERE lc2_perc = '3';
UPDATE lucas2015 SET lc2_perc = '3'  WHERE lc2_perc = '4';
UPDATE lucas2015 SET lc2_perc = '4'  WHERE lc2_perc = '5';
UPDATE lucas2015 SET lc2_perc = '5'  WHERE lc2_perc = '6';
UPDATE lucas2015 SET lc2_perc = '5'  WHERE lc2_perc = '7';
ALTER TABLE lucas2015 ADD COLUMN lu1_perc_cls varchar;
UPDATE lucas2015 SET lu1_perc_cls = lu1_perc;
UPDATE lucas2015 SET lu1_perc = '5'   WHERE lu1_perc_cls = '1';
UPDATE lucas2015 SET lu1_perc = '10'  WHERE lu1_perc_cls = '2';
UPDATE lucas2015 SET lu1_perc = '25'  WHERE lu1_perc_cls = '3';
UPDATE lucas2015 SET lu1_perc = '50'  WHERE lu1_perc_cls = '4';
UPDATE lucas2015 SET lu1_perc = '75'  WHERE lu1_perc_cls = '5';
UPDATE lucas2015 SET lu1_perc = '90'  WHERE lu1_perc_cls = '6';
UPDATE lucas2015 SET lu1_perc = '100' WHERE lu1_perc_cls = '7';
ALTER TABLE lucas2015 ADD COLUMN lu2_perc_cls varchar;
UPDATE lucas2015 SET lu2_perc_cls = lu2_perc;
UPDATE lucas2015 SET lu2_perc = '5'   WHERE lu2_perc_cls = '1';
UPDATE lucas2015 SET lu2_perc = '10'  WHERE lu2_perc_cls = '2';
UPDATE lucas2015 SET lu2_perc = '25'  WHERE lu2_perc_cls = '3';
UPDATE lucas2015 SET lu2_perc = '50'  WHERE lu2_perc_cls = '4';
UPDATE lucas2015 SET lu2_perc = '75'  WHERE lu2_perc_cls = '5';
UPDATE lucas2015 SET lu2_perc = '90'  WHERE lu2_perc_cls = '6';
UPDATE lucas2015 SET lu2_perc = '100' WHERE lu2_perc_cls = '7';
UPDATE lucas2015 SET lc_lu_special_remark = '22' WHERE lc_lu_special_remark = '1';
UPDATE lucas2015 SET lc_lu_special_remark = '1' WHERE lc_lu_special_remark = '2';
UPDATE lucas2015 SET lc_lu_special_remark = '2' WHERE lc_lu_special_remark = '22';
UPDATE lucas2015 SET lc_lu_special_remark = '100' WHERE lc_lu_special_remark = '7';
UPDATE lucas2015 SET lc_lu_special_remark = '-1' WHERE lc_lu_special_remark = '8';
UPDATE lucas2015 SET lc_lu_special_remark = '8' WHERE lc_lu_special_remark = '9';
UPDATE lucas2015 SET lc_lu_special_remark = '9' WHERE lc_lu_special_remark = '10';
UPDATE lucas2015 SET lc_lu_special_remark = '10' WHERE lc_lu_special_remark = '100';
UPDATE lucas2015 SET crop_residues = '22' WHERE crop_residues = '1';
UPDATE lucas2015 SET crop_residues = '1' WHERE crop_residues in ('2','3','4');
UPDATE lucas2015 SET crop_residues = '2' WHERE crop_residues = '22';
UPDATE lucas2015 SET obs_type = '-1' WHERE obs_type = '5';
UPDATE lucas2015 SET obs_type = '5' WHERE obs_type = '6';
ALTER TABLE lucas2015 ADD COLUMN office_pi varchar(1);
UPDATE lucas2015 SET office_pi = '0';
UPDATE lucas2015 SET office_pi = '1' WHERE obs_type = '7';
UPDATE lucas2015 SET pi_extension = '0' WHERE pi_extension = '';

--
-- No LC harmonization applied
--

--
-- LU harmonization
--
-- LU1
UPDATE lucas2015 SET lu1_h = '-1' WHERE lu1 IN ('U340', 'U410');
-- LU2
UPDATE lucas2015 SET lu2_h = '-1' WHERE lu2 IN ('U340', 'U410');
