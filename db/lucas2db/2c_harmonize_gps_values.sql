--
-- 2006
--
UPDATE lucas2006 SET gps_ew = '-1' WHERE gps_ew = 'X';
UPDATE lucas2006 SET gps_ew = '1' WHERE gps_ew = 'E';
UPDATE lucas2006 SET gps_ew = '2' WHERE gps_ew = 'W';
UPDATE lucas2006 SET gps_proj = '2' WHERE gps_proj = '0';

--
-- 2009
--
UPDATE lucas2009 SET gps_ew = '1' WHERE gps_ew = 'E';
UPDATE lucas2009 SET gps_ew = '2' WHERE gps_ew = 'W';
UPDATE lucas2009 SET gps_proj = '2' WHERE gps_proj = 'X';
UPDATE lucas2009 SET gps_proj = '1' WHERE gps_proj = 'WG';
UPDATE lucas2009 SET th_ew = '1' WHERE th_ew = 'E';
UPDATE lucas2009 SET th_ew = '2' WHERE th_ew = 'W';

--
-- 2012
--
UPDATE lucas2012 SET gps_ew = '1' WHERE gps_ew = 'E';
UPDATE lucas2012 SET gps_ew = '2' WHERE gps_ew = 'W';

--
-- 2015
--
UPDATE lucas2015 SET gps_ew = '1' WHERE gps_ew = 'E';
UPDATE lucas2015 SET gps_ew = '2' WHERE gps_ew = 'W';
UPDATE lucas2015 SET gps_lat = '-1' WHERE gps_lat = '0.0';
UPDATE lucas2015 SET gps_long = '-1' WHERE gps_long = '0.0';

--
-- 2018
--
UPDATE lucas2018 SET gps_lat = '-1' WHERE gps_lat = '0.0';
UPDATE lucas2018 SET gps_long = '-1' WHERE gps_long = '0.0';

-- fix coordinates for 2009
-- https://gitlab.com/geoharmonizer_inea/planning/-/issues/49#note_406829967
UPDATE lucas2009 SET th_long = th_long * -1 FROM
 (SELECT ogc_fid AS id FROM lucas2009 WHERE th_ew = '2') AS s
 WHERE ogc_fid = s.id;
UPDATE lucas2009 SET geog_th = st_setsrid(st_point(th_long,th_lat),4326) FROM
 (SELECT ogc_fid AS id FROM lucas2009 WHERE th_ew = '2') AS s
 WHERE ogc_fid = s.id;
