ST_LUCAS
========

The use of in-situ references in the Earth observation monitoring is a
fundamental need. `LUCAS
<https://ec.europa.eu/eurostat/web/lucas/overview>`__ (Land Use and
Coverage Area frame Survey) is an activity that performs repeated
in-situ surveys over Europe every three years since 2006. The dataset
is unique in many aspects, however, it is currently not available
through a standardized interface, machine-to-machine. Moreover, the
evolution of the surveys limits the change analysis. Therefore the
ST_LUCAS system was developed to fill these gaps.

The ST_LUCAS is designed as a multi-layer client-server system
allowing to be integrated into end-to-end workflows. It provides data
through an OGC compliant interface. Moreover, a geospatial user can
integrate the data through a `Python API <./api/>`__ to ease the use in
the workflows with spatial, temporal, attribute and thematic
filters. The client Python API can be used directly by the Python code
(CLI), running the `Jupyter Notebook <./api/tutorials.html>`__ or the
`QGIS plugin <./qgis_plugin/>`__ (GUI).

.. image:: _static/st_lucas_architecture.png
   :width: 600px

.. todo:: link to the paper

Useful links
------------

* `System metadata <./st_lucas_metadata.json>`__
* `System logs <./logs/>`__
* `Data extracts <./extracts/>`__

Harmonized LUCAS attributes
---------------------------

.. toctree::
            
   tables_rst/list_of_attributes

Coding tables
-------------

.. toctree::
            
$tables_coding_rst

Harmonization mapping tables
----------------------------

.. toctree::
            
$tables_mapping_rst
          
Funding
-------

This work is co-financed under Grant Agreement Connecting Europe
Facility (CEF) Telecom `project 2018-EU-IA-0095
<https://ec.europa.eu/inea/en/connecting-europe-facility/cef-telecom/2018-eu-ia-0095>`_
by the European Union.

.. image:: _static/CEF_programme_logo_650px.png
