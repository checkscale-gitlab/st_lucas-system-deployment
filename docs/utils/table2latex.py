#!/usr/bin/python3

# Convert CSV file to LaTeX notation
# Usage:
# ./utils/table2latex.py ../db/lucas2db/conf/harmonize_attributes.csv /tmp/harmonize_attributes.tex "Harmonization of LUCAS attributes" harm_attr

import argparse
import csv
from pathlib import Path

def csv2tex(input_file, caption):
    output_file = Path("/tmp") / (Path(input_file).stem + '.tex')
    label = Path(input_file).stem
    if caption is None:
        caption = label.upper().replace('_', '\_') + ' attribute'
        
    with open(input_file) as f:
        reader = csv.reader(f, delimiter=';')
        with open(output_file, "w", newline='') as tex:
            tex.write(f"% python3 utils/table2latex.py {input_file} {caption}\n")
            tex.write(f"""\\begin{{table}}[H] 
\caption{{{caption}.\label{{tab:{label}}}}}
\\newcolumntype{{C}}{{>{{\centering\\arraybackslash}}X}}\n""")
            header = next(reader)
            cc = 'C' * len(header)
            tex.write(f"""\\begin{{tabularx}}{{\\textwidth}}{{{cc}}}
\\toprule\n""")
            tex.write("   &   ".join(map(lambda x: "\\textbf{{{}}}".format(x.replace('_', '\_').title()), header)))
            tex.write("\\\\\n")
            tex.write("\midrule\n")
            for line in reader:
                tex.write("   &   ".join(map(lambda x: x.replace('_', '\_'), line)))
                tex.write(" \\\\\n")
            tex.write("""\\bottomrule
\end{tabularx}
\end{table}\n""")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('csv_file', metavar='csv_file', type=str,
                        help='Path to input CSV file')
    parser.add_argument('caption', metavar='caption', type=str,
                        help='Caption', nargs='?')
    args = parser.parse_args()

    csv2tex(args.csv_file, args.caption)

